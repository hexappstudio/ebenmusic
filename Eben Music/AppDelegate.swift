//
//  AppDelegate.swift
//  Eben Music
//
//  Created by 1 on 1/3/17.
//  Copyright © 2017 Absolute. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //Setting Status Bar Color
        UIApplication.shared.statusBarStyle = .lightContent
        
        //Setting Tab Bar
        setupTabBar()
        
        //Setting Tab Bar Items
        setTabBarItemsCustomParams()
        
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func setupTabBar() {
        
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().barTintColor = UIColor("#162631")
        UITabBar.appearance().tintColor = .white
    }

    func setTabBarItemsCustomParams(){
        
        guard let tabBarController = self.window?.rootViewController as? UITabBarController,
            let tabBarItem0 = tabBarController.tabBar.items?[0],
            let tabBarItem1 = tabBarController.tabBar.items?[1],
            let tabBarItem2 = tabBarController.tabBar.items?[2],
            let tabBarItem3 = tabBarController.tabBar.items?[3],
            let tabBarItem4 = tabBarController.tabBar.items?[4]
            else { return }
        
        setForTabBarItemImage(tabBarItem: tabBarItem0, imageForSelectedState: #imageLiteral(resourceName: "music_selected"), forNormalState: #imageLiteral(resourceName: "music"))
        setForTabBarItemImage(tabBarItem: tabBarItem1, imageForSelectedState: #imageLiteral(resourceName: "favorite_selected"), forNormalState: #imageLiteral(resourceName: "favorite"))
        setForTabBarItemImage(tabBarItem: tabBarItem2, imageForSelectedState: #imageLiteral(resourceName: "search_selected"), forNormalState: #imageLiteral(resourceName: "search"))
        setForTabBarItemImage(tabBarItem: tabBarItem3, imageForSelectedState: #imageLiteral(resourceName: "events"), forNormalState: #imageLiteral(resourceName: "events"))
        setForTabBarItemImage(tabBarItem: tabBarItem4, imageForSelectedState: #imageLiteral(resourceName: "more_selected"), forNormalState: #imageLiteral(resourceName: "more"))
        
        UITabBarItem.appearance().titlePositionAdjustment = UIOffsetMake(0, -2)
    }
    
    func setForTabBarItemImage(tabBarItem item: UITabBarItem, imageForSelectedState selected: UIImage, forNormalState normal: UIImage){
        item.selectedImage = selected.withRenderingMode(.alwaysOriginal)
        item.image = normal.withRenderingMode(.alwaysOriginal)
        item.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.white], for: UIControlState.normal)
    }

}

