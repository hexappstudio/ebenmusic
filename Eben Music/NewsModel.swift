//
//  NewsModel.swift
//  Eben Music
//
//  Created by 1 on 1/4/17.
//  Copyright © 2017 Absolute. All rights reserved.
//

import Foundation

struct NewsItem {
    let image: String
    let title: String
    let date: String
}
