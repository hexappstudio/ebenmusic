//
//  BaseViewController.swift
//  Eben Music
//
//  Created by 1 on 1/3/17.
//  Copyright © 2017 Absolute. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift

class BaseViewController: UIViewController {
    

    var titleImageLogo = "" {
        didSet{
            navigationItem.titleView = customImageView
        }
    }
    

    var titleForNavigation = "" {
        didSet{
                navigationItem.titleView = customTitleView
        }
    }
    
    lazy var customImageView:UIImageView = {
        let navLogo = UIImageView(frame: CGRect(x: 0, y: 0, width: 38, height: 38))
        navLogo.contentMode = .scaleAspectFit
        navLogo.image = UIImage(named: self.titleImageLogo)
        
        return navLogo
    }()
    
    lazy var customTitleView:UILabel = {
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
        titleLabel.numberOfLines = 2
        titleLabel.textColor = .white
        titleLabel.textAlignment = .center
        titleLabel.font = .boldSystemFont(ofSize: 18)
        titleLabel.text = self.titleForNavigation
        
        return titleLabel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor("#1b2e3d")
        
        self.navigationController?.navigationBar.barTintColor = UIColor("#162631")
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.isTranslucent = false

        // Do any additional setup after loading the view.
        setup()
    }

    func setup(){}

    
}
