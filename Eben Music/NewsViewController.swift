//
//  NewsViewController.swift
//  Eben Music
//
//  Created by 1 on 1/4/17.
//  Copyright © 2017 Absolute. All rights reserved.
//

import UIKit

class NewsViewController: BaseViewController {

    @IBOutlet weak var newsCollectionViewOutlet: UICollectionView!
    
    
    lazy var dataManager:DataManager = {
        return DataManager(withDelegate: self)
    }()
    
    lazy var cellSize: CGFloat = {return self.newsCollectionViewOutlet.frame.size.width*0.9}()

    var newsItemsArray: [EbenMusicItem]?{
        didSet{
        self.newsCollectionViewOutlet.reloadData()
        }
    }
    
    
    override func setup() {
        titleForNavigation = "NEWS"
        newsCollectionViewOutlet.reloadData()
        
        dataManager.getNews()
        
        
    }

}

extension NewsViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        return newsItemsArray?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! NewsCollectionViewCell
        
        cell.setupNewsCell(withItem: (newsItemsArray?[indexPath.row])!)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        
        
        return CGSize(width: cellSize, height: cellSize)
        
    }
    
}

extension NewsViewController: DataManagerDelegate
{
    func reciveNewsItems(_ ebenMusicItems:[EbenMusicItem]){
        print(ebenMusicItems)
        newsItemsArray = ebenMusicItems

        
    }
    func toggleError(_ error:Error){
        
    }
}


