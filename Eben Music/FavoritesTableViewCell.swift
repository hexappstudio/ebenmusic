//
//  FavoritesTableViewCell.swift
//  Eben Music
//
//  Created by 1 on 1/3/17.
//  Copyright © 2017 Absolute. All rights reserved.
//

import UIKit

class FavoritesTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewOutlet: UIImageView!
    @IBOutlet weak var songTitleLabelOutlet: UILabel!
    @IBOutlet weak var countryLabelOutlet: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
